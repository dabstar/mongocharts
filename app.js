// app.js

// BASE SETUP
// =============================================================================

// call the packages we need
var express    = require('express'), 		// call express
    mongojs = require("mongojs");			// call mongojs

var app = express(); 				// define our app using express

var port = process.env.PORT || 8888; 

var uri = "mongodb://boxuser:Boxpass123@ds039550.mongolab.com:39550/statistics_poc", 
 	 db = mongojs.connect(uri, ["inpoc"]);


// ROUTES FOR OUR API
// =============================================================================
	app.use(express.static(process.cwd() + '/public'));

	var router = express.Router();	

	router.use(function(req, res, next) {
	  console.log('%s %s %s', req.method, req.url, req.path);
	  next();
	});

    // set up the routes themselves
  	router.get('/', function(req, res) {
  		console.log('Time started: %s', new Date());
		var aggres = db.inpoc.aggregate(		
		{
			$unwind: "$product_list.BI"
		},
		{
			$project: {
				expiry_year: {$year: "$quote_expiry_date"},
				expiry_month: {$month: "$quote_expiry_date"},
				price: "$product_list.BI.product.yearly_price"			
			}
		},
		{
			$group: { 
				_id: {expiry_year: "$expiry_year", expiry_month: "$expiry_month"},
				// _id : "$Customer.FirstAndMiddleName",
				//_id: "$product_list.BI.reg_no",
				total: { $sum: "$price"},
				antal: { $sum: 1}
			}
		},
		{
			$sort: {expiry_month:1}
		}	
		,function(err, docs) {
			if(err) 
				console.log(err);
			 console.log('Time finishe:d %s', new Date());
			res.json(docs);
		});	
	});

	// app.get('*', function(req, res) {
 //            res.sendfile('./public/index.html'); // load our public/index.html file
 //        });

  	app.use('/api', router);
	app.listen(port);

	console.log('Magic happens on port ' + port);
