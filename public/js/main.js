var app = angular.module('myApp', ['nvd3ChartDirectives']);

app.controller('MyController', function($scope,$http) {

	$http.get('/api').success(function(data, status, headers, config) {
		// data contains the response
		// status is the HTTP status
		// headers is the header getter function
		// config is the object that was used to create the HTTP request
		$scope.data = data;

	}).error(function(data, status, headers, config) {
		$scope.data = data;
	});
});

app.controller('ExampleCtrl', function($scope, $http){
	$http.get('/api').success(function(data, status, headers, config) {
		// data contains the response
		// status is the HTTP status
		// headers is the header getter function
		// config is the object that was used to create the HTTP request
		$scope.chartData = [
     		{
 	        	"key": "Antal tilbud",
        	     "values": data
 			}
		];


	 	$scope.xFunction = function(){
	                return function(d) {
	                    return d._id.expiry_month;
	                };
	            }
		$scope.yFunction = function(){
	                return function(d) {
	                    return d.antal;
	                };
		}
	}).error(function(data, status, headers, config) {
		
	});
})

